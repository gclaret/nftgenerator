using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CaptureScreenshot : MonoBehaviour
{
    private List<Image> _images;
    
    private List<string> _layerNames = new List<string>();

    private Dictionary<string, List<Sprite>> _layersCachedContent = new Dictionary<string, List<Sprite>>();

    private int _amountExported = 1;
    
    void Start()
    {
        Application.runInBackground = true;

        CheckOutputFolderExists();
        
        CacheImageComponents();
        
        GetLayerNames();

        CacheLayersContent();

        StartCoroutine(GenerateNFTs());
    }

    private void CheckOutputFolderExists()
    {
        var directoryInfo = new DirectoryInfo("_Output/");
        if (!directoryInfo.Exists)
        {
            directoryInfo.Create();
        }
    }

    private void CacheImageComponents()
    {
        _images = GetComponentsInChildren<Image>().ToList();
    }

    private void GetLayerNames()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            _layerNames.Add(transform.GetChild(i).name);
        }
    }
    
    private void CacheLayersContent()
    {
        List<Sprite> loadedSprites;
        foreach (var layer in _layerNames)
        {
            loadedSprites = new List<Sprite>();
            
            var directoryInfo = new DirectoryInfo($"{Application.dataPath}/Resources/{layer}");
            var filesInDirectory = directoryInfo.GetFiles().Where(fileName => !fileName.Name.EndsWith(".meta"));;
            foreach (var file in filesInDirectory)
            {
                loadedSprites.Add(Resources.Load<Sprite>($"{layer}/{file.Name.Replace(".png", "")}"));
            }
            
            _layersCachedContent.Add(layer, loadedSprites);
        }
    }
    
    
    private IEnumerator GenerateNFTs()
    {
        var totalCombinations = 1;
        foreach (var cachedLayersKvp in _layersCachedContent)
        {
            totalCombinations *= cachedLayersKvp.Value.Count;
        }
        
        Debug.Log($"Total Combinations: {totalCombinations}");

        yield return StartCoroutine(SetImage(0));
    }

    IEnumerator SetImage(int currentLayer)
    {
        if(currentLayer < _layerNames.Count)
        {
            var currentLayerImages = _layersCachedContent[_layerNames[currentLayer]];
            foreach (var layerImage in currentLayerImages)
            {
                _images[currentLayer].sprite = layerImage;

                yield return StartCoroutine(SetImage(currentLayer + 1));
            }
        }
        else
        {
            yield return new WaitForEndOfFrame();

            SaveNFT();

            yield return new WaitForEndOfFrame();
        }
    }
    
    void SaveNFT()
    {
        ScreenCapture.CaptureScreenshot($"_Output/{_amountExported}.png");
        _amountExported++;
    }
}
